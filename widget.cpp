#include "widget.h"

#include <QFile>
#include <QDir>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include <QLayout>
#include <QPushButton>

#include "questionsinfo.h"
#include "questionwidget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    loadFile();

    setMinimumSize(300, 300);


    auto mainlay=new QVBoxLayout(this);

    m_marathonBtn=new QPushButton("Marathon", this);
    m_examBtn = new QPushButton("Exam", this);

    mainlay->addWidget(m_marathonBtn);
    mainlay->addWidget(m_examBtn);

    m_examBtn->setEnabled(false);


    connect(m_marathonBtn, &QPushButton::clicked, [this]{

        QuestionWidget w;

        w.setQuestions(m_questions);

        w.exec();
    });
}

Widget::~Widget()
{
}


void Widget::loadFile()
{

    QFile tmpFile(":/files/questionsFile");

    auto loadFileName=qApp->applicationDirPath()+QDir::separator()+"questions.json";
//    auto loadFileName="questions.json";

    if(!QFile::exists(loadFileName))
        tmpFile.copy(loadFileName);


    QFile loadFile(loadFileName);


    if(!loadFile.open(QFile::ReadOnly))
        return;

    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));


    auto questArr = loadDoc.object()["Questions"].toArray();

    m_questions.clear();


    for(int index=0;index<questArr.size();++index){
        QuestionsInfo *quest=new QuestionsInfo;

        quest->read(questArr.at(index).toObject());

        m_questions.append(quest);
    }


}
