#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QList>

class QuestionsInfo;
class QPushButton;

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();


private:

    void loadFile();

    QPushButton *m_marathonBtn;
    QPushButton *m_examBtn;

    QList<QuestionsInfo *>m_questions;
};
#endif // WIDGET_H
