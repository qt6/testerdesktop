#ifndef QUESTIONWIDGET_H
#define QUESTIONWIDGET_H

#include <QDialog>
#include <QList>

class QuestionsInfo;
class AnswersInfo;

class QPushButton;
class QFormLayout;
class QCheckBox;
class QTextEdit;
class QLabel;
class QVBoxLayout;


struct AnswerCheck
{
    AnswersInfo *answerInfo;
    QCheckBox *checkBox;
    QTextEdit *textEdit;

};

class QuestionWidget : public QDialog
{
    Q_OBJECT
public:
    explicit QuestionWidget(QWidget *parent = nullptr);

    void setQuestions(const QList<QuestionsInfo *> &questions);

private slots:

    void updateRight();
    void updateFields();

private:

    void addRow(AnswersInfo *ansInfo = nullptr);
    void clearRows();

    QList<AnswerCheck*>m_rows;


    QList<QuestionsInfo*>m_questions;
    QuestionsInfo *m_info;
    QuestionsInfo *m_chekedInfo;
    AnswersInfo *m_ansInfo;

    QVBoxLayout *mainLay;
    QFormLayout *m_answLay;
    QTextEdit *m_questW;
    QPushButton *m_applyBtn;
    QLabel *m_countRightLbl;
    QLabel *m_countQuestionsLbl;
    QCheckBox*m_showRight;

    int m_currentIndex;
    int m_countRights;

};

#endif // QUESTIONWIDGET_H
