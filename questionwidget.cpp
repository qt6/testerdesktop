#include "questionwidget.h"

#include <QDebug>
#include <QLayout>
#include <QLabel>
#include <QFormLayout>
#include <QTextEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QRandomGenerator>

#include "questionsinfo.h"
#include "answersinfo.h"

QuestionWidget::QuestionWidget(QWidget *parent)
    : QDialog{parent}
{

    setMinimumSize(640, 480);

    m_currentIndex = 0;
    m_countRights = 0;

    mainLay=new QVBoxLayout(this);
    m_answLay = new QFormLayout(this);

    m_applyBtn=new QPushButton("Check", this);

    m_chekedInfo = new QuestionsInfo;

    m_questW=new QTextEdit(this);
    m_countRightLbl = new QLabel("Count rights: 0", this);
    m_countQuestionsLbl = new QLabel("Count questions left: 0",this);

    m_showRight=new QCheckBox("Show rights", this);

    m_questW->setReadOnly(true);

    mainLay->addWidget(m_countQuestionsLbl);
    mainLay->addWidget(m_questW);
    mainLay->addLayout(m_answLay);
    mainLay->addWidget(m_applyBtn);
    mainLay->addWidget(m_countRightLbl);
    mainLay->addWidget(m_showRight);

    connect(m_showRight, &QCheckBox::stateChanged, [this](bool isCheked){
        if(isCheked){
            for(auto row: m_rows){
                row->checkBox->setChecked(row->answerInfo->isRight());
            }
        }else{
            for(auto row: m_rows){
                row->checkBox->setChecked(false);
            }
        }
    });

    connect(m_applyBtn, &QPushButton::clicked, [this]{



        int tmpRight = 0;
        for(auto row:m_rows){
            if(row->checkBox->isChecked()==row->answerInfo->isRight())
                ++tmpRight;
        }

        if(tmpRight==m_rows.size())
            ++m_countRights;

        m_countRightLbl->setText(QString("Count rights: %0").arg(m_countRights));

        m_questions.removeAt(m_currentIndex);
        m_applyBtn->setEnabled(m_questions.size());

        updateFields();
    });

}

void QuestionWidget::setQuestions(const QList<QuestionsInfo *> &questions)
{
    m_questions = questions;
    updateFields();

}

void QuestionWidget::updateRight()
{
    if(auto box = qobject_cast<QCheckBox*>(sender())){

    }
}

void QuestionWidget::updateFields()
{
    m_countQuestionsLbl->setText(QString("Count questions left: %0").arg(m_questions.size()));

    if(m_questions.isEmpty()){
        m_countRightLbl->setText(QString("Count rights: %0").arg(m_countRights));
        return;
    }

    m_currentIndex = QRandomGenerator::global()->bounded(m_questions.size());

    m_info = m_questions.at(m_currentIndex);

    m_questW->setText(m_info->question());

    clearRows();

    auto m_answers = m_info->answers();
    while(m_answers.size()){
        auto randomIndex=QRandomGenerator::global()->bounded(m_answers.size());
        addRow(m_answers.at(randomIndex));
        m_answers.removeAt(randomIndex);
    }


    m_showRight->setChecked(false);


}
void QuestionWidget::clearRows()
{
    while(m_answLay->count())
        m_answLay->removeRow(0);
    m_rows.clear();

}

void QuestionWidget::addRow(AnswersInfo *ansInfo)
{

    AnswerCheck *checker = new AnswerCheck;
    QCheckBox *tmpBox = new QCheckBox (this);
    QTextEdit *tmpText = new QTextEdit (this);

    tmpText->setReadOnly(true);

    if(ansInfo){
        //        tmpBox->setChecked(ansInfo->isRight());
        tmpText->setText(ansInfo->answer());
    }


    checker->answerInfo = ansInfo;
    checker->checkBox = tmpBox;
    checker->textEdit = tmpText;

    m_answLay->addRow(tmpBox, tmpText);
    m_rows.append(checker);

    connect(tmpBox, &QCheckBox::stateChanged, this, &QuestionWidget::updateRight);
}
